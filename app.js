const express = require('express');
const mongoose = require('mongoose');

const taskRouter = require('./routers/task');
const userRouter = require('./routers/user');
const balancedRouter = require('./routers/balanced');

const app = express();
mongoose.connect('mongodb://localhost:27017/bigApp', { useNewUrlParser: true });

app.use(express.json());


app.use(taskRouter);
app.use(userRouter);
app.use(balancedRouter);


const port = process.env.PORT || 3000;
app.listen(port,() => {
    console.log(`Server is running on port ${port}`);
});