const monggose = require('mongoose');

const balancedSchema = new monggose.Schema({
    message: {
        type: String,
        default: 'Failed',
        required: true
    },
    attempts: {
        type: Number,
        default: 0
    },
    user: {
        type: monggose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    }
});

const Balanced =  monggose.model('Balanced', balancedSchema);    

module.exports = Balanced;