const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const Balanced = require('../models/balanced');

const messageHandle = async (id, message) => {
    const response = await Balanced.findOne({ user: id }).populate('user', 'username').exec();
    response.attempts = response.attempts + 1;
    response.message = message;
    await response.save();
    return response;
}

router.post('/balanced', auth, async (req,res) => {
        const input = req.body.input;
        let exp = [];
        try {
            const user = await Balanced.findOne({ user: req.user._id });
            if(!user){
                const user = new Balanced({
                    user: req.user._id
                });
                await user.save();
            }
        } catch (e) {
            res.sendStatus(400).send();
        }
        for(let s in input){
            const p = input[s];
            if(p === '{' || p === '[' || p === '('){
                exp.push(p);
                //console.log(exp);
            }else if(p === '}' || p === ']' || p === ')'){
                const lastElement = exp[exp.length - 1];
                if ((lastElement === '{' && p === '}') || (lastElement === '[' && p === ']') || (lastElement === '(' && p === ')')){
                    exp.pop();
                }else{
                   const response = await messageHandle(req.user._id, 'Failed');
                   return  res.json(response);
                }
            }
        }
        if(exp.length === 0){
            const response = await messageHandle(req.user._id, "Success");
            return res.json(response);
        }else{
            const response = await messageHandle(req.user._id, 'Failed');
            return res.json(response);
        }
});


module.exports = router;