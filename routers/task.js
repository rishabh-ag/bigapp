const express = require('express');
const router = express.Router();

const Task = require('../models/task');
const auth = require('../middleware/auth');

router.post('/task', auth, async (req, res) => {
    const task = new Task({
        ...req.body,
        user: req.user._id
    });
    try {
       await task.save();
        res.json(task);
    } catch (e) {
        res.sendStatus(400).send(e);
    }
});

router.get('/task', auth, async (req,res) => {
    try {
        //const task = await Task.find();
        //res.json(task);
        const task = await Task.find({user: req.user._id}).populate('user', 'username').exec();
        res.json(task);
    } catch (e) {
        res.sendStatus(400).send(e);
    }
});

router.put('/task/:id', auth, async (req,res) => {
    try {
        const task = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.json(task);
    } catch (e) {
        res.sendStatus(401).send(e);
    }
});

router.delete('/task/:id', auth, async (req,res) => {
    try {
        await Task.findByIdAndDelete(req.params.id);
        res.sendStatus(200);
    } catch (e) {
        res.sendStatus(401).send(e);
    }
});

module.exports = router;