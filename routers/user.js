const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const User = require('../models/user');
const auth = require('../middleware/auth');


router.post('/users', async (req, res) => {
    const user = new User(req.body);
    try {
        const token = jwt.sign({ _id: user._id.toString() }, 'winterishere');
        user.tokens = user.tokens.concat({ token });
        await user.save();
        res.status(201).send({ user, token });
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/users/login', async (req, res) => {
    try {
        username =  req.body.username;
        const user = await User.findOne({ username });
        if (!user) {
            throw new Error('Unable to login');
        }
        const isMatch = await bcrypt.compare(req.body.password, user.password);
        if (!isMatch) {
            throw new Error('Unable to login');
        }
        const token = jwt.sign({ _id: user._id.toString() }, 'winterishere');
        user.tokens = user.tokens.concat({ token });
        await user.save();
        res.send({ message: "success", token });
    } catch (e) {
        res.status(400).send(e.message);
    }
});

router.get('/users', auth, async (req,res) => {
    try {
        if (req.user.role === 'admin') {
            const user = await User.find();
            res.json(user);
        } else {
            res.send('Not Authorized');
        }
    } catch (e) {
        res.sendStatus(400).send(e);
    }
});

router.delete('/users/:id', auth, async (req, res) => {
    try {
        console.log(req.user);
        if(req.user.role === 'admin') {
            const user = await User.findByIdAndDelete(req.params.id);
            if (!user) {
                throw new Error("User doesn't exits");
            }
            res.sendStatus(200);
        }else{
            throw new Error('Not Authorized');
        }
    } catch (e) {
        res.status(400).send(e.message);
    }
});


router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token;
        })
        await req.user.save();
        res.send();
    } catch (e) {
        res.status(500).send();
    }
});



module.exports = router;